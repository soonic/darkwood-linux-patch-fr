# - English

**This patch is intended to be used with Darkwood version 1.2 - Linux/GOG**

**Credit goes to [OkaXo](https://github.com/OkaXo/Darkwood-French-Translation) (Windows patch) for the translation**

---

## Prerequisites

`xdelta3` is required for the script to be able to apply the patch.

On Debian based distributions, it can be installed like this:

```bash
$ sudo apt install xdelta3
```

## Quick start

```bash
$ ./patch.sh --help
```

## Patching: Step by step instructions

1. [Download](https://bitbucket.org/soonic/darkwood-linux-patch-fr/downloads/) the archive

2. Extract it:  
`$ tar xvf darkwood-linux-patch-fr.tar.gz`

3. cd into the extracted dir:  
`$ cd darkwood-linux-patch-fr`

4. Make sure the scripts can be executed:  
`$ chmod +x patch.sh restore.sh`

5. Launch the patch script (replace the path with the actual path to your game installation):  
`$ ./patch.sh ~/GOG\ Games/Darkwood`

6. Enjoy Darkwood in French :)

## Restore original file (unpatch)

Launch the restore script (replace the path with the actual path to your game installation):  
`$ ./restore.sh ~/GOG\ Games/Darkwood`

## Mac and Steam version

If anyone can test this patch on Mac and on the Steam version, please provide me instructions, I will gladly add them here.

---

# - Français

**Ce patch est compatible avec Darkwood version 1.2 - Linux/GOG**

**Merci à [OkaXo](https://github.com/OkaXo/Darkwood-French-Translation) (patch Windows) pour les traductions**

## Prérequis

Le script d'installation à besoin du programme `xdelta3`.

Avec une distribution Linux basée sur Debian, il peut être installé de cette façon:

```bash
$ sudo apt install xdelta3
```

## Démarrage rapide

```bash
$ ./patch.sh --help
```

## Appliquer le patch: Instructions pas à pas

1. [Télécharger](https://bitbucket.org/soonic/darkwood-linux-patch-fr/downloads/) l'archive

2. Extraire:  
`$ tar xvf darkwood-linux-patch-fr.tar.gz`

3. Entrer dans le répertoire extrait:  
`$ cd darkwood-linux-patch-fr`

4. S'assurer que les scripts sont exécutables:  
`$ chmod +x patch.sh restore.sh`

5. Lancer le script pour appliquer le patch (remplacer le chemin par le chemin vers l'installation du jeu):  
`$ ./patch.sh ~/GOG\ Games/Darkwood`

6. Bon jeu en français :)

## Restaurer l'original

Lancer le script pour restaurer le fichier original (remplacer le chemin par le chemin vers l'installation du jeu):  
`$ ./restore ~/GOG\ Games/Darkwood`

## Mac et Steam

Si quelqu'un peut tester ce patch sur Mac et sur la version Steam, merci de me fournir les instructions d'installation, je les ajouterai ici.
