#!/bin/bash

echo "Patch FR pour Darkwood 1.2 linux (GOG)"
echo ""

if [ "$#" != "1" ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	echo "Utilisation : ./restore.sh [DIRECTORY]"
	echo "              DIRECTORY est le chemin vers le repertoire racine de Darkwood"
	echo ""
	echo "Exemple : ./restore.sh ~/GOG\ Games/Darkwood"
	echo ""
	exit 0
fi

GAME_DIR=`readlink -f "$1"`

TARGET_FILE="$GAME_DIR/game/Darkwood_Data/resources.assets"
BACKUP_FILE="$GAME_DIR/game/Darkwood_Data/.resources.assets"

HASH="454e0da226a4da2864e1695813a92ae779ab49ea"

if [ ! -f "$BACKUP_FILE" ]; then
	echo "Erreur: La copie de sauvegarde est introuvable: $BACKUP_FILE..."
	exit 1
fi

echo "Info: Vérification de la validité de la copie de sauvegarde..."
if [ `sha1sum "$BACKUP_FILE" | awk '{ print $1 }'` != $HASH ]; then
	echo "Erreur: Copie de sauvegarde invalide." >&2
	exit 1
fi

echo "Info: Restauration de la copie de sauvegarde..."
mv -f "$BACKUP_FILE" "$TARGET_FILE"
if [ $? == 1 ]; then
	echo "Erreur: Inpossible de remplacer le fichier cible avec la copie de sauvrgarde." >&2
	exit 1
fi

echo ""
echo "Copie de sauvegarde restaurée avec succès!"
echo ""
exit 0

