#!/bin/bash

echo "Patch FR pour Darkwood 1.2 linux (GOG)"
echo ""

if [ "$#" != "1" ] || [ "$1" == "--help" ] || [ "$1" == "-h" ]; then
	echo "Utilisation : ./patch.sh [DIRECTORY]"
	echo "              DIRECTORY est le chemin vers le repertoire racine de Darkwood"
	echo ""
	echo "Exemple : ./patch.sh ~/GOG\ Games/Darkwood"
	echo ""
	exit 0
fi

GAME_DIR=`readlink -f "$1"`
SCRIPT_DIR="$( cd "$(dirname "$0")" ; pwd -P )"

TARGET_FILE="$GAME_DIR/game/Darkwood_Data/resources.assets"
BACKUP_FILE="$GAME_DIR/game/Darkwood_Data/.resources.assets"
PATCH_FILE="$SCRIPT_DIR/resources.assets.xdelta"
PATCHED_FILE="$SCRIPT_DIR/resources.assets.patched"

TARGET_HASH="454e0da226a4da2864e1695813a92ae779ab49ea"
PATCHED_HASH="8178d4ef717dfb31a79630fe4f1db4c3c58d21bf"

if ! [ -x "$(command -v xdelta3)" ]; then
	echo "Erreur: xdelta3 n'est pas installé." >&2
	exit 1
fi

if [ ! -f "$TARGET_FILE" ]; then
	echo "Erreur: Le fichier cible n'existe pas: $TARGET_FILE." >&2
	exit 1
fi

echo "Info: Vérification de la validité du fichier original..."
if [ `sha1sum "$TARGET_FILE" | awk '{ print $1 }'` != $TARGET_HASH ]; then
	echo "Erreur: Fichier original invalide." >&2
	echo "        Assurez-vous que vous tentez de patcher la version 1.2 de Darkwood," >&2
	echo "        et que le fichier n'a pas déjà été modifié (déjà patché?)." >&2
	exit 1
fi

echo "Info: creation d'une copie de sauvegarde..."
cp "$TARGET_FILE" "$BACKUP_FILE"

echo "Info: Application du patch..."
xdelta3 -d -s "$TARGET_FILE" "$PATCH_FILE" "$PATCHED_FILE"
if [ $? == 1 ]; then
	rm "$PATCHED_FILE"
	echo "Erreur: xdelta3 n'a pas pu appliquer le patch." >&2
	exit 1
fi

echo "Info: Vérification de la validité du fichier patché..."
if [ `sha1sum "$PATCHED_FILE" | awk '{ print $1 }'` != $PATCHED_HASH ]; then
	echo "Erreur: Fichier patché invalide. Abandon." >&2
	rm "$PATCHED_FILE"
	exit 1
fi

echo "Info: Copie du fichier patché vers le répertoire du jeu..."
mv -f "$PATCHED_FILE" "$TARGET_FILE"
if [ $? == 1 ]; then
	echo "Erreur: Inpossible de remplacer le fichier cible avec le fichier patché.." >&2
	rm "$PATCHED_FILE"
	exit 1
fi

echo ""
echo "Patch appliqué avec succès!"
echo ""
exit 0

